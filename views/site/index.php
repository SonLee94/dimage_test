<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Dimage Share News';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1><?= $this->title ?></h1>
    </div>

    <div class="body-content">
        <?php if (!empty($posts)) : ?>
            <div class="row">
                <?php foreach ($posts as $post) : ?>
                    <div class="col-lg-6">
                        <h2><?= Html::encode($post['title']) ?></h2>
                        <p><?= ($post['description']) ?></p>

                        <p><?= Html::a('Xem chi tiết', ['/site/view', 'id' => $post['post_id']], ['class' => 'btn btn-default']) ?></p>
                    </div>
                <?php endforeach ?>
            </div>
        <?php endif ?>
    </div>
</div>
