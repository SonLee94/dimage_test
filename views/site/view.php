<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = $model->title;
?>
<div class="site-index">

    <div class="jumbotron">
        <h1><?= Html::encode($model->title) ?></h1>
    </div>

    <div class="body-content">
        <?php if (!empty($model)) : ?>
            <div class="row">
                <div class="col-lg-12">
                    <?= $model->content ?>
                </div>
            </div>
        <?php endif ?>
    </div>
</div>
