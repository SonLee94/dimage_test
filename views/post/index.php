<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Posts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Post', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',
            'description',
            [
                'label' => 'action',
                'value' => function($model) {
                    if ($model->published_f === \app\models\Post::PUBLISHED) {
                        return Html::a('Hạ bài', ['/post/down-post', 'id' => $model->post_id], [
                            'class' => 'btn btn-sm btn-danger',
                            'data-confirm' => 'Bạn có muốn hạ bài này không?',
                            'data-method' => 'post',
                        ]);
                    }

                    return Html::a('Đăng bài', ['/post/publish', 'id' => $model->post_id], ['class' => 'btn btn-sm btn-primary', 'data-method' => 'post',]);
                },
                'format' => 'raw',
                'visible' => Yii::$app->user->can('admin')
            ],
            // 'updated_date',
            // 'created_by',
            // 'updated_by',
            // 'published_f',
            // 'deleted_f',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
