<?php

use yii\db\Migration;

class m180725_050322_create_post extends Migration
{
    public function up()
    {
        $this->createTable('post', [
            'post_id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'description' => $this->string(),
            'content' => $this->text(),
            'created_date' => $this->integer(),
            'updated_date' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'published_f' => $this->smallInteger(),
            'deleted_f' => $this->smallInteger()
        ]);
    }

    public function down()
    {
        $this->dropTable('post');
    }
}
