<?php

use yii\db\Migration;

class m180725_034616_create_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'username' => $this->string(100),
            'email' => $this->string(150)->notNull(),
            'authKey' => $this->string(32)->notNull(),
            'password_hash' => $this->string(32)->notNull(),
            'access_token' => $this->string(100)->defaultValue(null),
            'is_active' => $this->integer(1),
            'is_admin' => $this->integer(1),
        ]);

        $this->batchInsert('user', [
            'id', 'username', 'email', 'authKey', 'password_hash', 'access_token', 'is_active', 'is_admin'
        ], [
            [1, 'admin', 'admin@gmail.com', 'edzHxX6v6rLuhfyEkUm2LQdSglR8o8BR', md5(123456), '', 1, 1],
            [2, 'author', 'author@gmail.com', 'tewHxX6v6rLabcyEkUm2LQdSglR8o7t4', md5(123456), '', 1, 0]
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user');
        $this->dropTable('user_info');
    }
}
