<?php

namespace app\models;

use app\components\Model;

class User extends Model implements \yii\web\IdentityInterface
{
    CONST ACTIVE = 1;
    CONST INACTIVE = 0;

    public $permission;
    public $password;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'password_hash'], 'required'],
            [['email'], 'email'],
            [['username', 'email'], 'unique'],
            [['username', 'password_hash', 'authKey'], 'string', 'max' => 250],
            [['email'], 'string', 'max' => 150],
            [['is_active', 'is_admin'], 'integer'],
            [['permission', 'password'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Username',
            'email' => 'Email',
            'is_active' => 'Is Active',
            'is_admin' => 'Is Admin',
        ];
    }

    public static function findIdentity($id)
    {
        $user = self::find()->where(['id' => $id])->one();

        if (!count($user)) {
            return null;
        }

        return new static($user);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $userType = null)
    {
        $user = self::find()
            ->where(['accessToken' => $token])
            ->one();

        if (!count($user)) {
            return null;
        }

        return new static($user);
    }

    /**
     * Finds user by username
     *
     * @param  string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        $user = self::find()
            ->where([
                'username' => $username,
                'is_active' => self::ACTIVE
            ])
            ->one();

        if (!count($user)) {
            return null;
        }

        return new static($user);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password_hash === md5($password);
    }

    public function isAdmin()
    {
        return $this->isAdmin === 1;
    }

    public function getRole()
    {
        return $this->hasOne(AuthAssignment::className(), ['user_id' => 'id']);
    }

    public function beforeSave($insert)
    {
        if (!empty($this->password)) {
            $this->password_hash = md5($this->password);
        }

        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->authKey = \Yii::$app->security->generateRandomString();
            }

            return true;
        }
        return false;
    }
}