<?php

namespace app\helpers;

use Yii;
use yii\helpers\Url;
use yii\imagine\Image;

/**
 * ImageHelper
 *
 * @author phulam
 */
class ImageHelper
{

    protected static $_fileinfo = null;

    /**
     * Resize and crop image to fit with $x and $y dimension
     * @param string $src
     * @param integer $x
     * @param integer $y
     * @return string cropped image path
     */
    public static function crop($src, $x, $y)
    {
        $srcPath = static::handlerSourcePath($src);

        if (file_exists($srcPath)) {
            self::$_fileinfo = pathinfo($src);
            $pathCode = static::getPathCode($x, $y);

            $dirname = '';

            if (isset(self::$_fileinfo['dirname']) && !empty(self::$_fileinfo['dirname'])) {
                $dirname = static::removeUploadPath(self::$_fileinfo['dirname']);
            }

            $cropPath = Yii::$app->params['cropPath'] . $pathCode . '/' . rtrim($dirname, '/\\');
            $cropFile = self::getCropFilePath($src, $x, $y, $cropPath);

            if (!is_dir($cropPath)) {
                mkdir($cropPath, 0755, true);
            }

            if (!file_exists($cropFile)) {
                Image::thumbnail(Yii::getAlias("@webroot/$srcPath"), $x, $y)
                ->save(Yii::getAlias("@webroot/$cropFile"), ['quality' => 80]);
            }

            return $cropFile;
        }

        return false;
    }

    /**
     * Create thumbnail
     * @param string $src source file without root upload path (example: /upload/filepath/file.ext, $src must be: filepath/file.ext)
     * @param integer $x width
     * @param integer $y height
     * @param string $imageId
     * @param string $force inputable value: auto, h, w
     * @return string thumbnail Path
     */
    public static function thumbnail($src, $x = null, $y = null)
    {
        $srcPath = static::handlerSourcePath($src);

        if (file_exists($srcPath)) {
            $pathCode = static::getPathCode($x, $y);

            self::$_fileinfo = pathinfo($src);

            $dirname = '';

            if (isset(self::$_fileinfo['dirname']) && !empty(self::$_fileinfo['dirname'])) {
                $dirname = static::removeUploadPath(self::$_fileinfo['dirname']);
            }

            $thumbPath = Yii::$app->params['thumbnailPath'] . $pathCode . '/' . rtrim($dirname, '/\\');
            $thumbFile = self::getThumbnailFilePath($src, $x, $y, $thumbPath);

            if (!is_dir($thumbPath)) {
                mkdir($thumbPath, 0755, true);
            }

            if (!file_exists($thumbFile)) {
                Image::thumbnail("@webroot/$srcPath", $x, $y)
                ->save(Yii::getAlias("@webroot/$thumbFile"), ['quality' => 85]);
            }

            return $thumbFile;
        }

        return false;
    }

    protected static function getThumbnailFilePath($src, $x = null, $y = null, $thumbPath = '')
    {
        if (empty(self::$_fileinfo)) {
            self::$_fileinfo = pathinfo($src);
        }

        $pathCode = static::getPathCode($x, $y);

        $thumbPath = trim($thumbPath) ? $thumbPath : (Yii::$app->params['thumbnailPath'] . $pathCode . '/' . rtrim(self::$_fileinfo['dirname'], '/\\'));
        $thumbFile = $thumbPath . '/' . self::$_fileinfo['filename'] . '.' . self::$_fileinfo['extension'];

        return $thumbFile;
    }

    public static function getThumbnailLink($src, $x = null, $y = null, $crop = false)
    {
        $uploadPath = Yii::$app->params['uploadPath'];
        $uploadPath = str_replace('/', '\/', $uploadPath);
        $src = preg_replace("/^({$uploadPath})/", '', $src);

        $pathCode = static::getPathCode($x, $y);

        if ($crop) {
            return Yii::$app->request->baseUrl . '/' . Yii::$app->params['cropPath'] . $pathCode . '/' . rtrim($src, '/\\');
        }

        return Yii::$app->request->baseUrl . '/' . Yii::$app->params['thumbnailPath'] . $pathCode . '/' . rtrim($src, '/\\');
    }

    protected static function getCropFilePath($src, $x = null, $y = null, $cropPath = '')
    {
        if (empty(self::$_fileinfo)) {
            self::$_fileinfo = pathinfo($src);
        }

        $pathCode = static::getPathCode($x, $y);

        $cropPath = trim($cropPath) ? $cropPath : (Yii::$app->params['cropPath'] . $pathCode . '/' . rtrim(self::$_fileinfo['dirname'], '/\\'));
        $cropFile = $cropPath . '/' . self::$_fileinfo['filename'] . '.' . self::$_fileinfo['extension'];

        return $cropFile;
    }

    public static function getAbsoluteAvatarUrl($url)
    {
        $url = static::handlerSourcePath($url);
        return Url::to(YiI::$app->request->baseUrl . '/', true) . '/' . $url;
    }

    /**
     * delete thumbnail
     * @param string $filePath
     * @param int $x 
     * @param int $y
     */
    public static function deleteThumbnail($avatar, $x, $y, $crop = false)
    {
        @unlink(ImageHelper::getThumbnailLink($avatar, $x, $y, $crop));
    }

    /**
     * Determine thumbnail is exist
     * @param $thumbnailPath
     * @return array
     */
    public static function isThumbnailExist($thumbnailPath)
    {
        return file_exists(Yii::getAlias('@webroot') . $thumbnailPath);
    }

    /**
     * Get path of thumb by width and height
     * @param type $x
     * @param type $y
     * @return type
     */
    protected static function getPathCode($x = null, $y = null)
    {
        $pathCode = 'freezie';

        if ($x !== null && $y !== null) {
            $pathCode = $x . '_' . $y;
        } else if ($x !== null && $y === null) {
            $pathCode = "w$x";
        } else if ($x === null && $y !== null) {
            $pathCode = "h$y";
        }

        return $pathCode;
    }

    /**
     * Handle src string to recorrect path
     * @param string $src
     * @return string
     */
    public static function handlerSourcePath($src)
    {
        if(empty($src)) {
            return;
        }

        $uploadPath = Yii::$app->params['uploadPath'];
        $src = static::removeUploadPath($src, $uploadPath);

        return ltrim(rtrim(Yii::$app->request->baseUrl, '\\/') . '/' . $uploadPath . $src, '/\\');
    }

    /**
     * Remove upload path from src path
     * @param string $src
     * @param string $uploadPath
     * @return string
     */
    public static function removeUploadPath($src, $uploadPath = null)
    {
        if(empty($src)) {
            return;
        }

        if (!$uploadPath) {
            $uploadPath = Yii::$app->params['uploadPath'];
        }

        if ($uploadPath) {
            $escapeUploadPath = str_replace('/', '\/', $uploadPath);
            return preg_replace("/^({$escapeUploadPath})/", '', $src);
        }

        return $src;
    }

}
