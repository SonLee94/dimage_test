<?php

namespace app\helpers;

use yii\bootstrap\Html;

/**
 * Description of MetronicTemplateHelper
 *
 * @author Lamnx <nguyenxuanlam1987@gmail.com>
 */
class MetronicTemplateHelper
{

    const radioBoxTemplate = '<div class="radio">{beginLabel}{input}{labelTitle}{endLabel}</div>';
    const checkBoxTemplate = '<div class="checkbox">{beginLabel}{input}{labelTitle}{endLabel}</div>';
    const checkBoxRadioContainer = false;
    const checkBoxRadioSep = false;

    public static function checkBoxOptions($model, $field, $labelOptions = ['class' => 'mt-checkbox'])
    {
        return [
            'labelOptions' => $labelOptions,
            'label' => $model->getAttributeLabel($field) . '<span></span>'
        ];
    }

    public static function checkBoxListOptions($wrapperClass = 'mt-checkbox-list', $labelClass = 'mt-checkbox mt-checkbox-outline')
    {
        return [
            'class' => $wrapperClass,
            'container' => false,
            'separator' => false,
            'item' => function ($index, $label, $name, $checked, $value) use ($labelClass) {
                return Html::label($label . ' ' . Html::checkbox($name, $checked, ['value' => $value]) . '<span></span>', null, ['class' => $labelClass]);
            },
        ];
    }
    
    public static function radioBoxOptions($model, $field, $labelOptions = ['class' => 'mt-radio'])
    {
        return [
            'labelOptions' => $labelOptions,
            'label' => $model->getAttributeLabel($field) . '<span></span>'
        ];
    }

    public static function radioBoxListOptions($wrapperClass = 'mt-radio-list', $labelClass = 'mt-radio mt-radio-outline')
    {
        return [
            'class' => $wrapperClass,
            'container' => false,
            'separator' => false,
            'item' => function ($index, $label, $name, $checked, $value) use ($labelClass) {
                return Html::label($label . ' ' . Html::radio($name, $checked, ['value' => $value]) . '<span></span>', null, ['class' => $labelClass]);
            },
        ];
    }

}
