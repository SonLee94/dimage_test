<?php

namespace app\helpers;

use yii\helpers\ArrayHelper as YiiArrayHelper;

/**
 * Extend of \yii\helpers\ArrayHelper
 *
 * @author Lamnx <nguyenxuanlam1987@gmail.com>
 */
class ArrayHelper extends YiiArrayHelper
{

    /**
     * Rebuild map to new index
     * ```php
     * $array = [
     *      ['id' => '123', 'name' => 'aaa', 'class' => 'x'],
     *      ['id' => '124', 'name' => 'bbb', 'class' => 'x'],
     *      ['id' => '345', 'name' => 'ccc', 'class' => 'y'],
     * ];
     * 
     * $result = ArrayHelper::mapToIndex($array, ['new_id' => 'id', 'new_name' => 'name']);
     * 
     * // the result is:
     * // [
     * //     ['new_id' => 123, 'new_name' => 'aaa'],
     * //     ['new_id' => 124, 'new_name' => 'bbb'],
     * //     ['new_id' => 345, 'new_name' => 'ccc'],
     * // ]
     * ```
     * @param array $array
     * @param array $keymap
     * @return array
     */
    public static function mapToIndex($array, $keymap, $group = null)
    {
        $result = [];

        foreach ($array as $index => $element) {
            foreach ($keymap as $newkey => $field) {

                if ($group !== null) {
                    $result[static::getValue($element, $group)][$newkey] = static::getValue($element, $field);
                } else {
                    $result[$index][$newkey] = static::getValue($element, $field);
                }
            }
        }

        return $result;
    }

    /**
     * Set value to key of array if array[key] is null
     * @param array $array
     * @param type $key
     * @param type $value
     * @return array
     */
    public static function setValue(array &$array, $key, $value)
    {
        if (is_null(static::getValue($array, $key))) {
            static::set($array, $key, $value);
        }

        return $array;
    }

    /**
     * Set an array item to a given value using "dot" notation.
     *
     * If no key is given to the method, the entire array will be replaced.
     *
     * @param  array   $array
     * @param  string  $key
     * @param  mixed   $value
     * @return array
     */
    static function set(&$array, $key, $value)
    {
        if (is_null($key)) {
            return $array = $value;
        }

        $keys = explode('.', $key);

        while (count($keys) > 1) {
            $key = array_shift($keys);
            // If the key doesn't exist at this depth, we will just create an empty array
            // to hold the next value, allowing us to create the arrays to hold final
            // values at the correct depth. Then we'll keep digging into the array.
            if (!isset($array[$key]) || !is_array($array[$key])) {
                $array[$key] = array();
            }
            $array = & $array[$key];
        }

        $array[array_shift($keys)] = $value;

        return $array;
    }

    /**
     * Return the default value of the given value.
     *
     * @param  mixed  $value
     * @return mixed
     */
    static function value($value)
    {
        return $value instanceof Closure ? $value() : $value;
    }

    /**
     * Build hierarchy nodes
     * @param array $nodes
     * @param type $parentId
     * @param type $depth
     * @return type
     */
    public static function buildHierarchyArray(array $items, $idField = 'id', $parentField = 'parent_id', $parentId = 0, $depth = 0)
    {
        $hierarchyItems = [];

        if ($depth == 0) {
            $items = static::getArrayHierarchy($items, $idField, $parentField);
        }

        if (empty($items[$parentId])) {
            return [];
        }

        foreach ($items[$parentId] as $item) {
            if (!empty($items[$item[$idField]])) {
                $item['items'] = static::buildHierarchyArray($items, $idField, $parentField, $item[$idField], $depth+1);
            }

            $hierarchyItems[$item[$idField]] = $item;
        }

        return $hierarchyItems;
    }

    /**
     * Gets an array representing the node hierarchy that can be traversed recursively
     * Format: item[parent_id][node_id] = node
     *
     * @param array|null Node list
     *
     * @return array Node hierarchy
     */
    public static function getArrayHierarchy($items, $idField = 'id', $parentField = 'parent_id')
    {
        $hierarchyItems = [];

        foreach ($items as $item) {
            $hierarchyItems[$item[$parentField]][$item[$idField]] = $item;
        }

        return $hierarchyItems;
    }
}
